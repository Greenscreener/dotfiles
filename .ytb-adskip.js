setInterval(() => {
	[...document.getElementsByTagName("ytd-reel-shelf-renderer")].forEach(e => e.parentElement.removeChild(e));  
}, 1000)

setInterval(() => {
	if (document.getElementsByClassName("ytp-ad-text").length !== 0 || document.getElementsByClassName("ytp-visit-advertiser-link").length !== 0) {
		document.querySelector("video.html5-main-video").playbackRate = 100;
	} else {
		if (document.querySelector("video.html5-main-video").playbackRate === 100) document.querySelector("video.html5-main-video").playbackRate = 1;
	}
}, 100)

setInterval(() => {
	if (document.getElementsByClassName("ytp-ad-skip-button-modern").length > 0) {
		document.getElementsByClassName("ytp-ad-skip-button-modern")[0].click()
	}
}, 30)
