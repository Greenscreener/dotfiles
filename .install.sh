#!/bin/bash


distsel () {
	if ! [[ -z "$OPENSUSE" ]]; then
		cut -f 1
	fi
}

apppl () {
	PACKAGELIST="$PACKAGELIST $(echo "$1 " | distsel)"
}

ask () {
	echo
	read -p "$1" -n 1 test
}

# Find OS in /etc/os-release
source /etc/os-release
if grep -ow "opensuse" <<< "$ID_LIKE" > /dev/null; then
	OPENSUSE=true
fi

# Confirm the found data with the user.
if ! [[ -z "$OPENSUSE" ]]; then
	read -p "Is your OS openSUSE? (y/n)" -n 1 test
	if [[ "$test" == n ]]; then
		unset OPENSUSE
	fi
fi
if [[ -z "$OPENSUSE" ]]; then
	read -p "No other OS than openSUSE is supported. Continue? (y/n)" -n 1 test
	if [[ "$test" == y ]]; then
		OPENSUSE=true
	else
		exit
	fi
fi

PACKAGELIST=""

# Ask user about bash and git (should be installed already)
ask "Install bash and git? (y/n)"
if [[ "$test" == y ]]; then
	BAG=true
	apppl "bash git"
fi

# Ask user about what packages to install
ask "Install zsh? (y/n)"
if [[ "$test" == y ]]; then
	ZSH=true
	apppl "zsh"
fi
ask "Install i3 desktop and GUI packages? (y/n)"
if [[ "$test" == y ]]; then
	I3DESKTOP=true
	printf "\nYou should install light using: \nsudo zypper addrepo https://download.opensuse.org/repositories/X11:Wayland/openSUSE_Tumbleweed/X11:Wayland.repo\nsudo zypper refresh\nsudo zypper install light"
	apppl "i3 i3blocks dunst fontawesome-fonts pavucontrol xbacklight flameshot libnotify-tools feh fira-code-fonts polkit-kde-agent-5 arandr xev konsole xautolock xdotool alacritty fira-code-fonts playerctl MozillaFirefox"
fi

ask "Install adapta and theme related packages? (y/n)"
if [[ "$test" == y ]]; then 
	ADAPTA=true
	apppl "metatheme-adapta-common papirus-icon-theme telegram-theme-adapta openbox-theme-adapta gtk4-metatheme-adapta kvantum-manager lxappearance qt5ct qt6ct"
fi

ask "Install KDE apps? (y/n)"
if [[ "$test" == y ]]; then 
	KDEAPPS=true
	apppl "dolphin kdeconnect-kde ark kate dolphin-tools"
fi

ask "Install plasmashell (for klipper xd)? (y/n)"
if [[ "$test" == y ]]; then
	PLASMA=true
	apppl "plasma5-workspace"
fi

ask "Install other useful (office) GUI apps? (y/n)"
if [[ "$test" == y ]]; then 
	OFFICE=true
	apppl "libreoffice gimp inkscape mpv"
fi

ask "Install note-taking apps? (y/n)"
if [[ "$test" == y ]]; then 
	NOTETAKING=true
	# TODO automate this 
	printf "\nYou should install typora from https://typora.io/linux/Typora-linux-x64.tar.gz "
	apppl "xournalpp"
fi

ask "Install very useful command-line tools? (y/n)"; 
if [[ "$test" == y ]]; then
	CLITOOLS=true
	apppl "pv htop git-lfs ncdu qrencode make jq fd inotify-tools entr exiftool fzf vim-fzf neovim xdpyinfo"
fi

ask "Install more nice-to-have command-line tools? (y/n)";
if [[ "$test" == y ]]; then
	MORECLITOOLS=true
	# TODO automate this
	apppl "hunspell-tools binwalk sshuttle speedtest-cli testdisk httpie whois figlet hollywood hugo"
fi


echo "Packages to install: "
echo "$PACKAGELIST"
