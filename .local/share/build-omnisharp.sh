#!/bin/bash

set -euo pipefail

pushd ~/.local/share/omnisharp-roslyn/
git checkout b2e64c6 -- $(find . -name CSharpDiagnosticWorkerWithAnalyzers.cs)
dotnet build --configuration Release
git reset
git checkout $(find . -name CSharpDiagnosticWorkerWithAnalyzers.cs)
popd
