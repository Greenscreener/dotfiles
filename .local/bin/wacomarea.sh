multiplier=$1
resolution=$(xdpyinfo | grep -oP 'dimensions.*pixels' | grep -oP '\d+x\d+')
width=$(echo $resolution | cut -d "x" -f 1)
height=$(echo $resolution | cut -d "x" -f 2)

xsetwacom set "Wacom One by Wacom M Pen stylus" Area 0 0 $(($width * $multiplier)) $(($height * $multiplier))
