#!/bin/bash
regex='Kensington Orbit Fusion Wireless Trackball\s+id=(\d\d)\s+\[slave +pointer +\(2\)]'
ids=$(xinput list | grep -oP "$regex" | perl -pe "s/$regex/\1/g" | tr "\n" " ")
actualId=0
maxLen=0
for id in $ids
do
	len=$(xinput list-props $id | wc -l)
	if (($len > $maxLen))
	then
		maxLen=$len
		actualId=$id
	fi
done
echo $actualId

xinput set-prop $actualId "libinput Accel Profile Enabled" 0 1 0
xinput set-prop $actualId "libinput Accel Speed" -.4
xinput set-prop $actualId "libinput Scroll Method Enabled" 0 0 1
