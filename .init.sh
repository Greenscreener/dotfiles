#!/bin/bash
set -e
if [ ! -d ".dotgit" ];
then
	git init --bare .dotgit
fi
dotgit="git --git-dir=$HOME/.dotgit --work-tree=$HOME"
read -p "Use git (s)sh or (h)ttps?" -n 1 test
echo
if [[ "$test" == "s" ]]; then
	remote="git@gitlab.com:/Greenscreener/dotfiles.git"
else
	remote="https://gitlab.com/Greenscreener/dotfiles.git"
fi
if $dotgit remote | grep origin >/dev/null;
then
	$dotgit remote remove origin
fi
$dotgit remote add origin $remote
$dotgit pull --set-upstream origin master
$dotgit submodule init
$dotgit submodule update
$dotgit config --local status.showUntrackedFiles no
