#!/bin/sh
if [ "$(cat /etc/prime/current_type)" == "nvidia" ]; 
then
    xrandr --output eDP-1-1 --primary --mode 1920x1080 --pos 1280x454 --rotate normal --output VGA-1-1 --off --output DP-1-1 --off --output HDMI-1-1 --off --output DP-1-2 --off --output HDMI-1-2 --off --output DP-1-2-1 --mode 1280x1024 --pos 0x0 --rotate normal --output DP-1-2-2 --off --output DP-1-2-3 --off;
else 
    xrandr --output eDP-1 --primary --mode 1920x1080 --pos 1280x454 --rotate normal --output VGA-1 --off --output DP-1 --off --output HDMI-1 --off --output DP-2 --off --output HDMI-2 --off --output DP-2-1 --mode 1280x1024 --pos 0x0 --rotate normal --output DP-2-2 --off --output DP-2-3 --off;
fi
