#!/usr/bin/env python3

import os
import re

body = os.environ["DUNST_BODY"]

output = re.sub(r'<[^>]+>', '', body.replace("<td>up</td>", "↑ ").replace("<td>down</td>", "↓ ").replace("<td>current</td>", "- ").replace("</tr>", "\n").replace("'", "&quot;")).strip()

os.system("dunstify -i clipboard -h string:synchronous:klipper-custom-clipboard-history -a 'klipper-custom-clipboard-history' 'Clipboard history' ' "+output+"'")
