#!/bin/bash

multiplier=28
if [ -f "$HOME/.config/i3/wcmmult" ];
then
    multiplier=$(cat ~/.config/i3/wcmmult)
fi

xsetwacom set "Wacom One by Wacom M Pen stylus" Button 2 pan
xsetwacom set "Wacom One by Wacom M Pen stylus" PanScrollThreshold 150
xsetwacom set "Wacom One by Wacom M Pen stylus" Mode Relative
~/.local/bin/wacomarea.sh $multiplier
