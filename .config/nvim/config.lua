require('nvim-treesitter.configs').setup({
	ensure_installed = {
		"arduino","awk","bash","bibtex","c","c_sharp","cmake","comment","cpp","css","dart","diff","dockerfile","dot","ebnf","fortran","fsharp","gdscript",
		"git_config","git_rebase","gitattributes","gitcommit","gitignore","go","gomod","gosum","gowork","graphql","haskell","haskell_persistent",
		"html","http","ini","java","javascript","jq","jsdoc","json","json5","jsonc","jsonnet","julia","kotlin","latex","llvm","lua","luadoc","luap","luau","make",
		"markdown","markdown_inline","matlab","mermaid","meson","ninja","nix","org","pascal","passwd","pem","perl","php","phpdoc","python","regex","ruby","rust","scss","solidity",
		"sql","todotxt","toml","typescript","vim","vimdoc","vue","yaml"},
	sync_install = true,
	highlight = {
		enable = true,
		additional_vim_regex_highlighting = false
	},
	indent = {
		enable = true
	},
	auto_install = true,
	incremental_selection = {
		enable = true,
		keymaps = {
			init_selection = '<c-space>',
			node_incremental = '<c-space>',
			scope_incremental = '<c-s>',
			node_decremental = '<M-space>',
		},
	},
	textobjects = {
		select = {
			enable = true,
			lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
			keymaps = {
				-- You can use the capture groups defined in textobjects.scm
				['aa'] = '@parameter.outer',
				['ia'] = '@parameter.inner',
				['af'] = '@function.outer',
				['if'] = '@function.inner',
				['aC'] = '@class.outer',
				['iC'] = '@class.inner',
				['ic'] = '@comment.inner',
				['ac'] = '@comment.outer',
				['aS'] = '@statement.outer',
			},
		},
		move = {
			enable = true,
			set_jumps = true, -- whether to set jumps in the jumplist
			goto_next_start = {
				[']m'] = '@function.outer',
				[']]'] = '@class.outer',
			},
			goto_next_end = {
				[']M'] = '@function.outer',
				[']['] = '@class.outer',
			},
			goto_previous_start = {
				['[m'] = '@function.outer',
				['[['] = '@class.outer',
			},
			goto_previous_end = {
				['[M'] = '@function.outer',
				['[]'] = '@class.outer',
			},
		},
		swap = {
			enable = true,
			swap_next = {
				['<leader>a'] = '@parameter.inner',
			},
			swap_previous = {
				['<leader>A'] = '@parameter.inner',
			},
		},
	}
})
-- 
-- require('nvim-treesitter.configs').setup({
--	rainbow = {
--		enable = true,
--		-- list of languages you want to disable the plugin for
--		disable = { },
--		-- Which query to use for finding delimiters
--		query = {
--			'rainbow-parens',
--			html = 'rainbow-tags',
--			latex = 'rainbow-blocks',
--		},
--		-- Highlight the entire buffer all at once
--		-- strategy = require('ts-rainbow').strategy.global,
--	}
-- })
--
-- This module contains a number of default definitions
local rainbow_delimiters = require 'rainbow-delimiters'

vim.g.rainbow_delimiters = {
	strategy = {
		[''] = rainbow_delimiters.strategy['global'],
	},
	query = {
		[''] = 'rainbow-delimiters',
		latex = 'rainbow-blocks',
		lua = 'ranbow-blocks'
	}
}
require('nvim-tree').setup()
require('lualine').setup()


local cmp = require('cmp')

cmp.setup({
	snippet = {
		expand = function(args)
			vim.fn["vsnip#anonymous"](args.body)
		end,
	},
	mapping = cmp.mapping.preset.insert({
		['<C-u>'] = cmp.mapping.scroll_docs(-4), -- Up
		['<C-d>'] = cmp.mapping.scroll_docs(4), -- Down
		-- C-b (back) C-f (forward) for snippet placeholder navigation.
		['<C-Space>'] = cmp.mapping.complete(),
		['<CR>'] = cmp.mapping.confirm {
			behavior = cmp.ConfirmBehavior.Replace,
			select = true,
		},
		['<Tab>'] = cmp.mapping(function(fallback)
			if cmp.visible() then
				cmp.select_next_item()
			else
				fallback()
			end
		end, { 'i', 's' }),
		['<S-Tab>'] = cmp.mapping(function(fallback)
			if cmp.visible() then
				cmp.select_prev_item()
			else
				fallback()
			end
		end, { 'i', 's' }),
	}),
	sources = cmp.config.sources({
		{ name = 'nvim_lsp' },
		{ name = 'vsnip' }
	}, {
		{ name = 'buffer' }
	})
})

require('nvim-autopairs').setup({})

local cmp_autopairs = require('nvim-autopairs.completion.cmp')
cmp.event:on('confirm_done', cmp_autopairs.on_confirm_done())


-- vim.lsp.set_log_level("info") 

local capabilities = require("cmp_nvim_lsp").default_capabilities()
local lspconfig = require('lspconfig')

-- Enable some language servers with the additional completion capabilities offered by nvim-cmp
local servers = { 'html', 'jsonls', 'cssls', 'eslint', 'clangd', 'texlab', 'arduino_language_server', 'rust_analyzer', 'bashls', 'fsautocomplete', 'dafny' }
for _, lsp in ipairs(servers) do
	lspconfig[lsp].setup {
		capabilities = capabilities,
	}
end
--- lspconfig.lua_ls.setup({
---		capabilities = capabilities,
---		settings = {
---			Lua = {
---				diagnostics = {
---					globals = {'vim'}
---				}
---			}
---		}
--- })
lspconfig.gopls.setup({
	capabilities = capabilities,
	settings = {
		gopls = {
			hoverKind = "Structured",
			staticcheck = true,
			completeUnimported = true,
			deepCompletion = true,
		}
	}
})
local pid = vim.fn.getpid()
lspconfig.omnisharp.setup({
	capabilities = capabilities,
	cmd = { "OmniSharp",	"--languageserver", "--hostPID", tostring(pid) }
})
lspconfig.basedpyright.setup {
	capabilities = capabilities,
	settings = {
		basedpyright = {
			analysis = {
				autoSearchPaths = true,
				diagnosticMode = "openFilesOnly",
				useLibraryCodeForTypes = true,
				typeCheckingMode = "basic"
			}
		}
	}
}
lspconfig.ruff.setup {
	capabilities = capabilities,
	init_options = {
		settings = {
			showSyntaxErrors = false,
			lint = {
				preview = true,
				select = {
					"F", -- flake8 rules
					"E", -- pycodestyle errors
					"W",  -- pycodestyle warnings
					"UP", -- pyupgrade
					"B", -- flake8-bugbear
					"RUF", -- ruff specific rules
				},
				ignore = {
					"W191", -- indentation contains tabs
					"E501", -- line too long
					"F401", -- imported but unused, already handled by pyright
					"F403", -- wildcard imports
					"F405", -- may be undefined because of wildcard import
					"F821", -- undefined name, already handled by pyright
					"F841", -- unused variable, already handled by pyright
				}
			},
		}
	}
}
-- require'lspconfig'.pylsp.setup{
--  capabilities = capabilities,
--  settings = {
--  	pylsp = {
--  		plugins = {
--  			pycodestyle = {
--  				ignore = { "W191" },
--  				maxLineLength = 9999,
--  			}
--  		}
--  	}
--  }
-- }
-- lspconfig.pyright.setup {
--	on_attach = on_attach,
--	capabilities = capabilities,
--	flags = lsp_flags,
--	settings = {
--		python = {
--			analysis = {
-- --				autoSearchPaths = true,
-- --				useLibraryCodeForTypes = true,
-- --				diagnosticMode = 'openFilesOnly',
--			},
--		},
--	},
-- --	root_dir = function(fname)
-- --		return util.root_pattern(".git", "setup.py", "setup.cfg", "pyproject.toml", "requirements.txt")(fname) or
-- --		util.path.dirname(fname)
-- --	end
-- }

-- Global mappings.
-- See `:help vim.diagnostic.*` for documentation on any of the below functions
vim.keymap.set('n', '<Leader>e', vim.diagnostic.open_float)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next)
vim.keymap.set('n', '<Leader>l', vim.diagnostic.setloclist)

-- Use LspAttach autocommand to only map the following keys
-- after the language server attaches to the current buffer
vim.api.nvim_create_autocmd('LspAttach', {
	group = vim.api.nvim_create_augroup('UserLspConfig', {}),
	callback = function(ev)
		-- Enable completion triggered by <c-x><c-o>
		vim.bo[ev.buf].omnifunc = 'v:lua.vim.lsp.omnifunc'

		-- Buffer local mappings.
		-- See `:help vim.lsp.*` for documentation on any of the below functions
		local opts = { buffer = ev.buf }
		vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, opts)
		vim.keymap.set('n', 'gd', vim.lsp.buf.definition, opts)
		vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts)
		vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, opts)
		vim.keymap.set('n', '<Leader>K', vim.lsp.buf.signature_help, opts)
		vim.keymap.set('n', '<Leader>wa', vim.lsp.buf.add_workspace_folder, opts)
		vim.keymap.set('n', '<Leader>wr', vim.lsp.buf.remove_workspace_folder, opts)
		vim.keymap.set('n', '<Leader>wl', function()
			print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
		end, opts)
		vim.keymap.set('n', '<Leader>D', vim.lsp.buf.type_definition, opts)
		vim.keymap.set('n', '<Leader>rn', vim.lsp.buf.rename, opts)
		vim.keymap.set({ 'n', 'v' }, '<Leader>ca', vim.lsp.buf.code_action, opts)
		vim.keymap.set('n', 'gr', vim.lsp.buf.references, opts)
		vim.keymap.set('n', '<Leader>f', function()
			vim.lsp.buf.format { async = true }
		end, opts)
	end,
})

require("fidget").setup({})
require('hop').setup()
require('nvim-ts-autotag').setup()

vim.g.barbar_auto_setup = false -- disable auto-setup

require'barbar'.setup {
	icons = {
		filetype = {
			enabled = false,
		}
	}
}

require("lsp_lines").setup()
vim.diagnostic.config({
	virtual_text = false,
	virtual_lines = { only_current_line = true }
})

require("Comment").setup()
require("ibl").setup()
