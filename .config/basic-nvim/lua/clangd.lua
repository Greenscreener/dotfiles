local client = vim.lsp.start({
	name = 'clangd',
	cmd = { 'clangd' },
})
vim.lsp.buf_attach_client(0, client)
vim.keymap.set('n', 'gD', vim.lsp.buf.declaration)
vim.keymap.set('n', 'gd', vim.lsp.buf.definition)
vim.keymap.set('n', 'K', vim.lsp.buf.hover)
vim.keymap.set('n', 'gi', vim.lsp.buf.implementation)
vim.keymap.set('n', '<Leader>K', vim.lsp.buf.signature_help)
vim.keymap.set('n', '<Leader>D', vim.lsp.buf.type_definition)
vim.keymap.set('n', '<Leader>rn', vim.lsp.buf.rename)
vim.keymap.set({ 'n', 'v' }, '<Leader>ca', vim.lsp.buf.code_action)
vim.keymap.set('n', 'gr', vim.lsp.buf.references)
vim.keymap.set('n', '<Leader>f', function() vim.lsp.buf.format { async = true } end)
vim.keymap.set('n', '<Leader>at', function() vim.lsp.buf_attach_client(0, client) end)

-- put this file into ~/.config/nvim/lua/clangd.lua, then, you can use
--	:lua require('clangd')
-- to start and connect the LSP and then use <Leader>at to attach it to each
-- buffer in which you want to use it
