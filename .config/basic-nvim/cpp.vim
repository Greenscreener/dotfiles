set nocompatible																		" be iMproved, required
set tabstop=4 shiftwidth=4 softtabstop=4 noexpandtab smarttab smartindent autoindent	" tabs are 4 chars wide, indent with tabs, automatically indent
set nowrap																				" do not soft-wrap too long lines, hide overflowing content
set cursorline number																	" highlight line with cursor, show line numbers
set hlsearch incsearch ignorecase smartcase												" highlight all search results, search when typing, ignore case when search pattern is lowercase
set encoding=utf-8																		"
set clipboard=unnamedplus																" bind system clipboard to vim clipboard
set wildmode=longest:full,full															" command mode tab completion behaves sanely
set updatetime=100																		" update editor every 100ms, calls plugins and LSPs

set listchars=tab:\|\ ,trail:▒,extends:»,precedes:«,nbsp:░								" show non-printable characters, tabs are indicated with |, overflowing lines with « and » and nobreakspaces and trailing whitespaces glow
set list																				" enable showing non-printable characters

set termguicolors																		" enable 24-bit color
set background=dark																		" prefer dark theme
if has('nvim')
	colorscheme lunaperche
else
	colorscheme industry
endif

syntax on																				" enable syntax highlighting

filetype plugin indent on																" enable filetype-aware indentation

vnoremap . :norm.<CR>																	" Use . in visual mode

" Use HJKL in normal mode with Ctrl
imap <C-H> <Left>
imap <C-J> <Down>
imap <C-K> <Up>
imap <C-L> <Right>

" set leader key to space
let mapleader=" "

" switch tabs quickly
map <C-H> gT
map <C-L> gt

" go 5 lines up and 5 lines down
map <C-J> 5j
map <C-K> 5k

" Ctrl-Backspace in normal mode
imap <C-BS> <C-w>
imap <C-h>  <C-w>

" close current buffer
map <Leader>d  <cmd>bd<CR>
" unhighlight last search
map <Leader>n  <cmd>nohl<CR>
" hard-wrap current paragraph
map <Leader>gq vipgq
" enable soft-wrap, do not break up words, continue with indentation
map <Leader>wl <cmd>set wrap! lbr bri<CR>
" copy entire file into clipboard
map <Leader>y  ggVGy

" completion options for LSPs and co, show menu even if only one completion exists to show hints
set completeopt=menu,menuone,popup

if has('nvim')
	" put swapfiles in a central directory
	let &directory=stdpath('config').'/swapfiles//'
	let &backupdir=stdpath('config').'/backupfiles//'
	" persist undo between runs
	let &undodir=stdpath('config').'/undofiles//'
	set undofile

	" quick escape from neovim's terminal
	tnoremap <C-w> <C-\><C-n><C-w>

else
	set directory=$HOME/.vim/swapfiles//
	set backupdir=$HOME/.vim/backupfiles//
	set undodir=$HOME/.vim/undofiles//
	set undofile
endif

" spell checker
set spelllang=en_gb,cs
map <Leader>s <cmd>set invspell<CR>
